#!/usr/bin/env python
# -*- coding: utf-8 -*-

from datetime import datetime

empty_field = "\uFFF0"

'''
Reimplementation of the Discord.py's Embed class.

-Fields can now be empty
-timestamps are added automatically to the footer text
'''


class Embed:
    def __init__(self):
        self.body = {}

    def set_title(self, title):
        self.body['title'] = title
        return self

    def set_color(self, color):
        self.body['color'] = color
        return self

    def set_description(self, description):
        self.body['description'] = description
        return self

    def set_thumbnail(self, url):
        self.body['thumbnail'] = {'url': url}
        return self

    def set_image(self, url):
        self.body['image'] = {'url': url}
        return self

    def with_author(self, content=None, url=None):
        self.body['author'] = {
            'name': content,
            'icon_url': url
        }
        return self

    def add_field(self, name=None, value=None, inline=False):
        if name is None:
            name = empty_field
        if value is None:
            value = empty_field

        fields = self.body.setdefault('fields', [])
        fields.append({
                'name': name,
                'value': value,
                'inline': inline
            })
        return self

    def set_footer(self, text=None, icon_url=None, timestamp=True):
        if timestamp is True:
            self.body['timestamp'] = datetime.utcnow().isoformat()

        self.body['footer'] = {
            'text': text,
            'icon_url': icon_url
        }
        return self

    def to_dict(self):
        return self.body

