#!/usr/bin/env python
# -*- coding: utf-8 -*-

import discord
import inspect
import aiohttp

from discord.ext import commands
from shared_classes.Embed import Embed


class BotCommands(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def giveid(self, ctx, user: discord.Member = None):
        if user is None:
            user = ctx.author
        await ctx.send(f"User `{user.name}` has the id: `{user.id}`")

    @commands.command()
    async def userinfo(self, ctx, user: discord.Member = None):
        if user is None:
            user = ctx.author

        await ctx.send(f"Name: `{user.name}` \nNickname: `{user.nick}`"
                       f"\nUser ID: `{user.id}` \nAccount created at: `{user.created_at}`")

    @commands.command()
    async def purge(self, ctx, amount: int = 10):
        if amount > 100:
            await ctx.send("You can only purge 100 messages at a time")
            return

        await ctx.message.delete()
        await ctx.channel.purge(limit=amount)
        await ctx.send(f"{amount} message(s) have been deleted!", delete_after=5)

    @commands.command(aliases=["av"])
    async def avatar(self, ctx, user: discord.User = None):
        if user is None:
            user = ctx.author

        em = Embed()
        em.set_image(url=str(user.avatar_url))
        em.set_footer(text=f"Avatar of user: {user.name}")

        await ctx.send(embed=em)

    def how_many_sec(self, time: int, types: str = None):
        hour = ['h', 'hour', 'hours']
        minute = ['m', 'min', 'minute', 'minutes']
        second = ['s', 'sec', 'second', 'seconds']

        if types is not None:
            if types.lower() in hour:
                seconds = int(time * 3600)
                return seconds

            elif types.lower() in minute:
                seconds = int(time * 60)
                return seconds

            elif types.lower() in second:
                return time
        return time

    @commands.command()
    async def slowmode(self, ctx, amount: int, types: str = None):
        if amount is None:
            amount = 0

        time = self.how_many_sec(amount, types)

        em = Embed()
        em.set_color(0xc0ffee)
        em.set_description(f"Slowmode is set to {time} seconds")
        em.set_footer(f"User {ctx.author.name} has changed the slowmode", str(ctx.author.avatar_url))

        await ctx.message.delete()
        await ctx.channel.edit(slowmode_delay=time)
        await ctx.send(embed=em)


def setup(bot):
    bot.add_cog(BotCommands(commands.Cog))
