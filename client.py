#!/usr/bin/env python
# -*- coding: utf-8 -*-

import logging
from discord.ext import commands


class Bot(commands.Bot):
    def __init__(self, bot_config):
        self.bot_config = bot_config
        self.logger = logging.getLogger('Bot')
        super().__init__(description='', command_prefix='>>', pm_help=True, case_insensitive=True)

    def run(self):
        token = self.bot_config['token']
        self.logger.info("Starting the bot!")
        super().run(token)
