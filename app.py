#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging
import asyncio
import discord
import traceback

import client

logging.basicConfig(level='INFO')

with open('extensions.json') as fp:
    extensions = json.load(fp)

with open('credentials.json') as fp:
    config = json.load(fp)

bot = client.Bot(config['bot'])

for extension in extensions:
    try:
        bot.load_extension(extension)

    except Exception as e:
        logging.error(f"Cannot load extension: '{extension}' with the following traceback:\n{traceback.format_exc()}")


@bot.event
async def on_ready():
    print('Online, ready to keep track of the levels!')

    while True:
        await bot.change_presence(activity=discord.Game(name="Helping people to make bots in Discord.py"))
        await asyncio.sleep(60)
        await bot.change_presence(activity=discord.Game(name="Testing bot stuff"))
        await asyncio.sleep(60)

bot.run()
